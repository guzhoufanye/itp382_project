﻿using UnityEngine;
using System.Collections;

public class DeadPlatform : MonoBehaviour {
	Cutscene cs;
	// Use this for initialization
	void Start () {
		cs = Camera.main.GetComponent<Cutscene> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			cs.GameOver();
			
		}
	}
}
