/// <summary>
/// Switch controller.
/// Game Developers Guild
/// Steve Peters
/// 3/28/2013
/// 
/// This script controls a single pole throw switch. The objects consist of a base and a switch
/// arm which is moved by the script 20degrees from center depending on the state of the switch
/// 
/// if this switch is used with the default Unity character controller, you have to add a 
/// collider set to trigger, and a rigidbody set to kinematic. You must also set a tag to Player if you want it to interact 
/// with the default Unity character controller
/// </summary>
using UnityEngine;
using System.Collections;
using System;//so that we can acess enum length

public class SwitchController : MonoBehaviour
{
	
	public Transform switchHandle;				//Should be set in the prefab
	public bool defaultOn = false;				//true if the switch is to be in the on position by default
	public float smooth = 0.75f;				//controls the speed of the switch handle movement
	public switchPosition _switchPos;			//can be referenced by an external script to control the switch
	public float switchDelayTime = 0.5f;		//Delay between switch operations. Prevents triggering the switch multiple times on one touch
	public SwitchReciever[] switchRecieverArray;//Populate this with object you want controlled by the switch
	public String[] collisionTag;

	//Bounds for the switch rotation, initial values are compensated for Blender rotation error
	public int posRotAmount = -70; 			//90 - 20 since default offset for Blender imports in 90 degrees and we want 20 degrees of travel for the switch handle
	public int negRotAmount = -110;			//90 + 20 since default offset for Blender imports in 90 degrees and we want 20 degrees of travel for the switch handle

	const float SWITCHANGLE = Mathf.Deg2Rad * 20;
	private float _SwitchDelayTimer;
	
	void Start ()
	{
		if (!defaultOn) {
			_switchPos = switchPosition.Off;
		} else {
			_switchPos = switchPosition.On;
			
		}
		ActivateSwitchReciever();			//Make sure the intended reciever function is in the required state
	}
	
	/// <summary>
	/// Update this instance.
	/// checks the state of the switch and calls the appropriate slerping function
	/// </summary>
	void Update ()
	{
		
		if (_switchPos == switchPosition.Off) {
			SwitchOff ();
		} else {
			SwitchOn ();
		}
	}
	
	/// <summary>
	/// Switchs the off. //leaving this in since it's hilarious
	/// rotates the switch handle to the off position
	/// </summary>
	public void SwitchOff ()
	{
	
		
		Quaternion target = Quaternion.Euler (new Vector3 (posRotAmount, 0, 0));
		switchHandle.localRotation = Quaternion.Slerp (switchHandle.localRotation, target, Time.deltaTime * smooth);
	}
	
	/// <summary>
	/// Switchs the on.
	/// rotates the switch handle to the on position
	/// </summary>
	public void SwitchOn ()
	{
		
		Quaternion target = Quaternion.Euler (new Vector3 (negRotAmount, 0, 0));
		switchHandle.localRotation = Quaternion.Slerp (switchHandle.localRotation, target, Time.deltaTime * smooth);
	}
	
	/// <summary>
	/// Raises the collision enter event.
	/// if the player hits the switch, switch the switch state
	/// </summary>
	/// <param name='col'>
	/// Col.
	/// </param>
	void OnCollisionEnter (Collision col)
	{
		for(int i = 0; i < collisionTag.Length; i++)
		{
		if (col.gameObject.tag == collisionTag[i]) {
			ChangeSwitchPosition ();
		}
		}
	}
	
	/// <summary>
	/// Raises the trigger enter event.
	/// if the player hits the switch, switch the switch state
	/// triggers must be used with default Unity character controller
	/// </summary>
	/// <param name='col'>
	/// Col.
	/// </param>
	void OnTriggerEnter (Collider col)
	{
		for(int i = 0; i < collisionTag.Length; i++)
		{
		if (col.gameObject.tag == collisionTag[i]) {
			ChangeSwitchPosition ();
		}
		}
	}
	
	/// <summary>
	/// Changes the switch position.
	/// </summary>
	void ChangeSwitchPosition ()
	{
		if (Time.time > _SwitchDelayTimer) {
			//reset delay time
			_SwitchDelayTimer = Time.time + switchDelayTime;
			
			//Play the switching sound
			if(audio != null)
			audio.Play();
			
			if (_switchPos == (switchPosition)Enum.GetValues (typeof(switchPosition)).Length - 1) {
				_switchPos = 0;
			} else {
				_switchPos++;
			}
			
			ActivateSwitchReciever();
		}
	}
	
	private void ActivateSwitchReciever()
	{
	for (int i = 0; i < switchRecieverArray.Length; i++) {
				
		if(switchRecieverArray[i] != null)
			{
		//if switch is on
		if(_switchPos == 0)
		{
			switchRecieverArray [i].OnSwitchedOn();
			}
		
		//else switch is off
		else
		{
			switchRecieverArray [i].OnSwitchedOff();
		}
			}
		}
	}
	
	/// <summary>
	/// Switch position.
	/// can be on, or even more suprisingly, it can also be off
	/// </summary>
	public enum switchPosition
	{
		On,
		Off
	};
}
