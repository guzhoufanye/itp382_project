﻿/// <summary>
/// Light switch receiver.
/// /// Game Developers Guild
/// Steve Peters
/// 3/28/2013
/// 
/// </summary>

using UnityEngine;
using System.Collections;

public class LightSwitchReceiver : SwitchReciever {

	public override void OnSwitchedOn()
	{
		light.enabled = true;
	}
	
	public override void OnSwitchedOff()
	{
		light.enabled = false;
	}
}
