﻿/// <summary>
/// Game object switch receiver.
/// /// Game Developers Guild
/// Steve Peters
/// 3/28/2013
/// 
/// </summary>

using UnityEngine;
using System.Collections;

public class GameObjectSwitchReceiver : SwitchReciever
{

	public override void OnSwitchedOn ()
	{
		gameObject.SetActive (true);
	}
	
	public override void OnSwitchedOff ()
	{
		gameObject.SetActive (false);
	}
}
