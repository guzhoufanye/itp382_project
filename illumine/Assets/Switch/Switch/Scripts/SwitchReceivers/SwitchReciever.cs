﻿/// <summary>
/// Switch reciever.
/// 11/1/2013
/// Game Developers Guild
/// Miami Fl
/// Steve Peters
/// 
/// The switch reciever is attached to an object you want to control with a GDG switch
/// use the switchtype enumeration to control the behaviour of the object
/// </summary>
using UnityEngine;
using System.Collections;

public class SwitchReciever : MonoBehaviour
{
	protected Transform _myTransform;
	//cache our transform position for efficiency
	void Start()
	{
		_myTransform = transform;
	}

	public virtual void OnSwitchedOn()
	{
		Debug.Log("Add Switch Function");
	}
	
	public virtual void OnSwitchedOff()
	{
		Debug.Log("Add Switch Function");
	}
}
