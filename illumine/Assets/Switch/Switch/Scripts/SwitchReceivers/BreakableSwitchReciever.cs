﻿using UnityEngine;
using System.Collections;

public class BreakableSwitchReciever : SwitchReciever {

	public BreakType breaktype = BreakType.standard;
	public Vector3 direction = new Vector3(100, 0 ,0);
	public Vector3 rotation = new Vector3(0, 100, 0);
	private bool _canBreak = true;


	public override void OnSwitchedOn ()
	{
		if (_canBreak) {
			//Uncomment these lines if you are using the GDG breakable framework
//			if (_myTransform.gameObject.activeSelf) {
//				if(breaktype == BreakType.standard)
//				{
//				_myTransform.gameObject.GetComponent<PhysicsController_Child> ().breakObject (true);
//				}
//
//				if(breaktype == BreakType.direction)
//				{
//					_myTransform.gameObject.GetComponent<PhysicsController_Child> ().breakObject (true, direction);
//				}
//
//				if(breaktype == BreakType.rotation)
//				{
//					_myTransform.gameObject.GetComponent<PhysicsController_Child> ().breakObject (true, direction, rotation);
//				}
//
//				_canBreak = false;
//			}
		}
	}
	
	public override void OnSwitchedOff ()
	{
	}

	public enum BreakType
	{
		standard,
		direction,
		rotation
	}
}
