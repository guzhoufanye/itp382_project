﻿/// <summary>
/// Renderer switch receiver.
/// /// Game Developers Guild
/// Steve Peters
/// 3/28/2013
/// 
/// </summary>
using UnityEngine;
using System.Collections;

public class RendererSwitchReceiver : SwitchReciever
{

	public override void OnSwitchedOn ()
	{
		transform.renderer.enabled = true;
	}
	
	public override void OnSwitchedOff ()
	{
		transform.renderer.enabled = false;
	}
}
