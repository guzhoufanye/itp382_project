﻿/// <summary>
/// Custom switch receiver.
/// /// Game Developers Guild
/// Steve Peters
/// 3/28/2013
/// 
/// </summary>

using UnityEngine;
using System.Collections;

public class CustomSwitchReceiver : SwitchReciever {
	
	public Color color1;
	public Color color2;
	
	public override void OnSwitchedOn()
	{
		transform.renderer.material.color = color1;
	}
	
	public override void OnSwitchedOff()
	{
		transform.renderer.material.color = color2;
	}
}
