﻿using UnityEngine;
using System.Collections;

public class ExplosionSwitchReciever : SwitchReciever
{
		private bool _canExplode = true;
	
		public override void OnSwitchedOn ()
		{
				if (_canExplode) {
			//Uncomment these lines if you are using the GDG breakable framework

//						//for some reason, we can still call methods on an object that is off
//						if (_myTransform.gameObject.activeSelf) {
//								_myTransform.gameObject.GetComponent<PhysicsController_Child> ().explodeObject ();
//								_canExplode = false;
//						}
			}
		}
	
		public override void OnSwitchedOff ()
		{
		}
}
