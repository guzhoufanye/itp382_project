﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerItem : MonoBehaviour {
	List< string > items;
	List< GameObject > itemObjects;
	public bool gun;
	public GameObject FlareGun;
	public Texture btnTexture;
	// Use this for initialization
	void Start () {
		items= new List< string >();
		itemObjects = new List<GameObject> ();
		gun = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddItem(string s, GameObject go){
		Debug.Log ("Pickkkkkk");
		items.Add (s);
		itemObjects.Add (go);
		gun = true;
	}

	void OnGUI(){
		foreach (string s in items){
			if (s == "Gun"){
				if (GUI.Button(new Rect(Screen.width-150, 50,100, 80), btnTexture)){

					UseGun(items.IndexOf (s));
					items[items.IndexOf (s)] = "Gunn";
				}
			}
		}
	}

	void UseGun(int index){
		//Debug.Log (index);

		GameObject go = itemObjects [index];
		FlareGun gun = go.GetComponent<FlareGun> ();
		Vector3 playerPosition = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z+0.9f);
		Debug.Log (playerPosition);
		PlayerController pc = this.gameObject.GetComponent<PlayerController> ();
		Quaternion rot;
		if (pc.left == true) {
			rot = new Quaternion(0,0,180,0);
		}
		else{
			rot = new Quaternion(0,0,0,0);
		}
		Instantiate(FlareGun, playerPosition, rot);
	}
}
