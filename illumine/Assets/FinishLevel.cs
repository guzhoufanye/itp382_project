﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour {
	int levelNum;

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			Debug.Log ("Level Finished!");
			Cutscene cs = Camera.main.GetComponent<Cutscene>();
			cs.OnFinishLevel();
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
