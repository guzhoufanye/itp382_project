﻿using UnityEngine;
using System.Collections;

public class Cutscene : MonoBehaviour {
	public GameObject CutSceneLight;
	public GameObject[] OtherLights;
	public bool finished;
	public bool paused;
	public Font MyFont;
	public Font TutorialFont;
	public int TutorialStage;
	public int LevelNumber;
	public bool gameover;
	public Texture btnTexture;
	// Use this for initialization
	void Start () {
		CutSceneLight.renderer.enabled = false;
		finished = false;
		//LevelNumber = 1;
		gameover = false;
		paused = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnFinishLevel(){
		CutSceneLight.renderer.enabled = true;
		for (int i=0; i<OtherLights.Length; i++){
			OtherLights[i].renderer.enabled =false;
		}
		GameObject l = GameObject.Find ("BgrLight");
		Light li = l.GetComponent<Light> ();
		li.intensity = 0;
		finished = true;
		TutorialStage = 100;
		Invoke ("LoadNewScene", 3f);
	}

	void LoadNewScene(){
		if (LevelNumber == 1){
			Application.LoadLevel ("Level2");
		}
		else if (LevelNumber == 2){
			Application.LoadLevel ("FlareFunAndRotatePlatform");
		}
		else if (LevelNumber == 3){
			Application.LoadLevel ("Level4");
		}
		else if (LevelNumber == 4){
			Application.LoadLevel ("Level5");
		}
		else if (LevelNumber == 5){
			Application.LoadLevel ("EndScene");
		}
	}

	public void PauseGame(){
		Debug.Log ("Cliked");
		paused = true;
		CutSceneLight.renderer.enabled = true;
		GameObject l = GameObject.Find ("BgrLight");
		Light li = l.GetComponent<Light> ();
		li.intensity = 0;
		GameObject p = GameObject.Find ("Player");
		Health h = p.GetComponent<Health> ();
		h.GamePaused (paused);
	}

	public void ContinueGame(){
		paused = false;
		CutSceneLight.renderer.enabled = false;
		GameObject l = GameObject.Find ("BgrLight");
		Light li = l.GetComponent<Light> ();
		li.intensity = 0.23f;
		GameObject p = GameObject.Find ("Player");
		Health h = p.GetComponent<Health> ();
		h.GamePaused (paused);
	}

	void OnGUI(){
		if (gameover){
			GUI.skin.font = MyFont;
			GUI.skin.label.fontSize = 80;
			GUI.Label (new Rect (Screen.width/2-150, Screen.height/2-50, 300, 100), "Game Over");
			GUI.skin.font = TutorialFont;
			GUI.skin.label.fontSize = 35;
			GUI.Label (new Rect (Screen.width/2-200, Screen.height/2+40, 400, 100), "Press any key to restart");
		}
		else{


			if (paused){
				GUI.skin.font = MyFont;
				GUI.skin.label.fontSize = 80;
				GUI.Label (new Rect (Screen.width/2-150, Screen.height/2-50, 300, 100), "Paused");
				GUI.skin.font = TutorialFont;
				GUI.skin.label.fontSize = 35;
				GUI.Label (new Rect (Screen.width/2-200, Screen.height/2+30, 400, 100), "Touch anywhere to continue");
			}

			if (!finished){
				GUI.skin.font = TutorialFont;
				GUI.skin.label.fontSize = 40;
				switch(TutorialStage){
					case 1:
						GUI.Label (new Rect (Screen.width/6, Screen.height/6, 300, 100), "Stay in light to gain health");
						break;
					case 2:
						GUI.Label (new Rect (Screen.width-300, 30, 300, 100), "Leading to another Journey...");
						break;
					case 0:
						GUI.Label (new Rect (10, Screen.height/2-20, 100, 50), "Left");
						GUI.Label (new Rect (Screen.width/2-50, Screen.height/2-20, 100, 50), "Jump");
						GUI.Label (new Rect (Screen.width-110, Screen.height/2-20, 100, 50), "Right");
						break;
					default:
						break;
				}
			}

			if (finished && LevelNumber != 5){
				//Debug.Log ("Finished");
				GUI.skin.font = MyFont;
				GUI.skin.label.fontSize = 60;
				GUI.Label (new Rect (Screen.width/2-150, Screen.height/2-50, 300, 100), "Next Adventure");
			}
		}	
	}

	public void GameOver(){
		Debug.Log ("Game Over");
		CutSceneLight.renderer.enabled = true;
		gameover = true;
		for (int i=0; i<OtherLights.Length; i++){
			OtherLights[i].renderer.enabled =false;
		}
		GameObject l = GameObject.Find ("BgrLight");
		Light li = l.GetComponent<Light> ();
		li.intensity = 0;
	}
}
