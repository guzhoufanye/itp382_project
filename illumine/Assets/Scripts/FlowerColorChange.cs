﻿using UnityEngine;
using System.Collections;

public class FlowerColorChange : MonoBehaviour {

	public Material colorMat;

	private float scale = 1f;
	// Update is called once per frame
	void Update () 
	{
		scale = Health.CurrentHealth / Health.MaxHealth;

		float g = scale;
		float r = 1f - scale;

		colorMat.color = new Color (r, g, 0);

	}
}
