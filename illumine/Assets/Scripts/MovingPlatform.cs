﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {
	public float speed;

	public Vector3 StartPoint;
	public Vector3 EndPoint;
	public bool Horizontal;
	// Use this for initialization
	void Start () {
		if(Horizontal){
			StartPoint.y = transform.position.y;
			EndPoint.y = transform.position.y;
		}
		else{
			StartPoint.x = transform.position.x;
			EndPoint.x = transform.position.x;
		}
	}

	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;

		transform.position = pos;
		if (Horizontal){
			pos.x += speed * Time.deltaTime;
			if (transform.position.x > EndPoint.x&&speed>0 || transform.position.x < StartPoint.x&&speed<0){
				speed = 0-speed;
			}
		}
		else{
			pos.y += speed * Time.deltaTime;
			if (transform.position.y >= EndPoint.y&&speed>0 || transform.position.y <= StartPoint.y&&speed<0){
				speed = 0-speed;
			}
		}
		transform.position = pos;
	}

	void OnCollisionEnter(Collision other){
		audio.Play ();
	}
}
