﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {
	public static float MaxHealth;
	public static float CurrentHealth;
	public float HealthRate;
	GameObject[] Lights;
	public bool Lighting;
	GameObject hs;
	Slider slider;
	Cutscene cs;
	bool pause;
	// Use this for initialization
	void Start () {
		MaxHealth = 100f;
		CurrentHealth = MaxHealth;
		//HealthRate = 2f;
		cs = Camera.main.GetComponent<Cutscene> ();
		Lights = cs.OtherLights;
		Lighting = false;
		hs = GameObject.Find ("HealthSlider");
		slider = hs.GetComponent<Slider> ();
		slider.maxValue = MaxHealth;
		pause = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			if (Lighting && CurrentHealth < MaxHealth){
				CurrentHealth += HealthRate * Time.deltaTime;
			}
			else{
				CurrentHealth -= HealthRate * Time.deltaTime;
			}
		}
		slider.value = CurrentHealth;
		if (CurrentHealth <= 0){
			cs.GameOver();
		}


	}

	public void InLight(bool i){
		Lighting = i;
	}

	public void GamePaused(bool p){
		pause = p;
	}
}
