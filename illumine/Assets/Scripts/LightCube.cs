﻿using UnityEngine;
using System.Collections;

public class LightCube : MonoBehaviour {
	public float speed;
	public float currentSpeed;
	public Vector3 StartPoint;
	public Vector3 EndPoint;
	public bool Horizontal;
	public bool RotateCube;
	public bool ShouldMove;
	public GameObject MyLight;
	public float distance = 2.0f;
	public bool LightInit;
	public bool right;
	// Use this for initialization
	void Start () {
		right = true;
		MyLight.renderer.enabled = LightInit;
		ShouldMove = false;
		currentSpeed = 0.0f;
		//speed = 2.0f;
		if(Horizontal){
			StartPoint.y = transform.position.y;
			EndPoint.y = transform.position.y;
			StartPoint.x = transform.position.x-distance;
			EndPoint.x = transform.position.x+distance;
		}
		else{
			StartPoint.x = transform.position.x;
			EndPoint.x = transform.position.x;
			StartPoint.y = transform.position.y-distance;
			EndPoint.y = transform.position.y+distance;
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = transform.position;
		CheckShouldMove ();

		if (ShouldMove){
			if (RotateCube){
				//Debug.Log ("Rotate");
				this.transform.Rotate(Vector3.back * speed * Time.deltaTime);
			}
			else{
				transform.position = pos;
				if (Horizontal){
					pos.x += speed * Time.deltaTime;
					if (transform.position.x > EndPoint.x&&speed>0 || transform.position.x < StartPoint.x&&speed<0){
						speed = 0-speed;
						right = !right;
					}
				}
				else{
					pos.y += speed * Time.deltaTime;
					if (transform.position.y >= EndPoint.y&&speed>0 || transform.position.y <= StartPoint.y&&speed<0){
						speed = 0-speed;
					}
				}
				transform.position = pos;
			}
		}
	}

	void CheckShouldMove(){
		if (RotateCube){

		}

		else{
			if (MyLight.renderer.enabled){
				ShouldMove = true;
				currentSpeed = speed;
			}
			else{
				currentSpeed = 0;
				ShouldMove = false;
			}
		}

	}

	void OnCollisionEnter(Collision other){
		audio.Play ();
	}
}
