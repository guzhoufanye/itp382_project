﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointLightBehavior : MonoBehaviour {

	public Material lightBoundMat;
	public float boundWidth = 0.1f;
	private int boundDivision = 60;
	// Use this for initialization
	void Start () 
	{
		setupBound ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		updateBound ();
	}

	void setupBound()
	{
		float PI = Mathf.PI;
		float x = 0;
		float y = 0;
		float r = this.light.range;
		
		Color c1 = Color.red;
		Color c2 = Color.green;
		
		float theta_scale = Mathf.Deg2Rad * (360f / boundDivision);//Set lower to add more points
		
		
		LineRenderer lineRenderer = this.gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = lightBoundMat;
		lineRenderer.SetColors(c1, c2);
		lineRenderer.SetWidth(boundWidth, boundWidth);
		lineRenderer.SetVertexCount(boundDivision + 1);
		
		int i = 0;
		for(float theta = 0; theta < 360 * Mathf.Deg2Rad; theta += theta_scale) 
		{
			x = r * Mathf.Cos(theta);
			y = r * Mathf.Sin(theta);
			
			Vector3 pos = new Vector3(this.transform.position.x + x, 
			                          this.transform.position.y + y, 
			                          0);
			lineRenderer.SetPosition(i, pos);
			i+=1;
		}
	}

	void updateBound()
	{
		LineRenderer lineRenderer = this.gameObject.GetComponent<LineRenderer>();
		float x = 0;
		float y = 0;
		float r = this.light.range;

		float theta_scale = Mathf.Deg2Rad * (360f / boundDivision);             //Set lower to add more points

		int i = 0;
		for(float theta = 0; theta < 360 * Mathf.Deg2Rad; theta += theta_scale) 
		{
			x = r * Mathf.Cos(theta);
			y = r * Mathf.Sin(theta);
			
			Vector3 pos = new Vector3(this.transform.position.x + x, 
			                          this.transform.position.y + y, 
			                          0);
			lineRenderer.SetPosition(i, pos);
			i+=1;
		}
	}

}
