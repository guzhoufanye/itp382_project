﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float horizontalMaxSpeed = 3f;
	public float jumpPower = 10f;
	public float moveForce;
	public bool movingOnPlatform;
	public bool movingOnVerticalPlatform;
	bool OnRotatePlatform;
	public bool left;


	private bool grounded = false;
	[HideInInspector]public bool leftBlocked = false;
	[HideInInspector]public bool rightBlocked = false;

	private Transform groundCheck;
	private Transform leftBotCheck;
	private Transform leftMidCheck;
	private Transform leftTopCheck;
	private Transform rightBotCheck;
	private Transform rightMidCheck;
	private Transform rightTopCheck;
	
	Cutscene cs;
	LightCube lc;



	void Awake()
	{
		this.transform.FindChild("Checks").gameObject.SetActive(true);

		groundCheck = this.transform.Find("Checks").transform.Find("groundCheck");
		leftBotCheck = this.transform.Find("Checks").transform.Find("leftBotCheck");
		leftMidCheck = this.transform.Find("Checks").transform.Find("leftMidCheck");
		leftTopCheck = this.transform.Find("Checks").transform.Find("leftTopCheck");
		rightBotCheck = this.transform.Find("Checks").transform.Find("rightBotCheck");
		rightMidCheck = this.transform.Find("Checks").transform.Find("rightMidCheck");
		rightTopCheck = this.transform.Find("Checks").transform.Find("rightTopCheck");

	}

	void Start () 
	{
		movingOnPlatform = false;
		renderer.enabled = false;
		cs = Camera.main.GetComponent<Cutscene> ();
		left = false;

		Transform robot = transform.GetChild (0);
		robot.GetComponent<Animator>().SetBool( "walk", false );
		robot.GetComponent<Animator>().SetBool( "jump", false );
		robot.GetComponent<Animator>().SetBool( "throw", false );
	}
	
	// Update is called once per frame
	void Update () 
	{
		Transform robot = transform.GetChild (0);
		if(!grounded){
			robot.GetComponent<Animator>().SetBool( "jump", false );
		}
		robot.GetComponent<Animator>().SetBool( "throw", false );
		grounded = Physics.Linecast (this.transform.position, groundCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain"));

		if(Physics.Linecast (this.transform.position, leftBotCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")) || 
		   Physics.Linecast (this.transform.position, leftMidCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")) ||
		   Physics.Linecast (this.transform.position, leftTopCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")))
		{
			leftBlocked = true;
		}
		else
		{
			leftBlocked = false;
		}

		if(Physics.Linecast (this.transform.position, rightBotCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")) || 
		   Physics.Linecast (this.transform.position, rightMidCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")) || 
		   Physics.Linecast (this.transform.position, rightTopCheck.position, 1 << LayerMask.NameToLayer ("RigidTerrain")))
		{
			rightBlocked = true;
		}
		else
		{
			rightBlocked = false;
		}



		//iPhone touch input

		bool leftChecked = false;
		bool midChecked = false;
		bool rightChecked = false;

		for(int i=0; i < Input.touchCount; i++){
			Touch t = Input.GetTouch (i);
			if(t.position.x < Screen.width/5){
				if(!leftChecked){
					leftChecked = true;
				}
			}
			else if(t.position.x > 4*Screen.width/5){
				if(t.position.y < (Screen.height-250) && !rightChecked){
					rightChecked = true;
				}

			}
			else if(!midChecked){
				midChecked = true;

			}
		}
		

		if (movingOnPlatform) {
			
			float s = 0-lc.currentSpeed;
			if (lc.right){
				s = 0-lc.currentSpeed;
			}
			this.transform.position += Vector3.left * s * Time.deltaTime;
		}
		
		if (movingOnVerticalPlatform) {
			
			float s = 0-lc.currentSpeed;
			if (lc.right){
				s = 0-lc.currentSpeed;
			}
			this.transform.position -= Vector3.up * s * Time.deltaTime;
		}


		if( (Input.GetKey(KeyCode.A)||Input.GetKey (KeyCode.LeftArrow)||leftChecked) && !leftBlocked )
		{
			transform.localEulerAngles = new Vector3(0,0,0);
			robot.GetComponent<Animator>().SetBool( "walk", true );
			this.transform.position += Vector3.left * horizontalMaxSpeed * Time.deltaTime;
			left = true;

		}
		else if((Input.GetKey(KeyCode.D)||Input.GetKey (KeyCode.RightArrow) || rightChecked)&& !rightBlocked )
		{
			transform.localEulerAngles = new Vector3(0,210,0);
			robot.GetComponent<Animator>().SetBool( "walk", true );
			this.transform.position += Vector3.right * horizontalMaxSpeed * Time.deltaTime;
			left = false;
		}
		else{
			robot.GetComponent<Animator>().SetBool( "walk", false );

		}
		if((Input.GetKeyDown(KeyCode.Space) || midChecked) && (grounded||OnRotatePlatform))
		{
			robot.GetComponent<Animator>().SetBool( "jump", true );
			robot.GetComponent<Animator>().SetBool( "walk", false );
			this.rigidbody.velocity += jumpPower * Vector3.up;
			if(this.rigidbody.velocity.y >= jumpPower){
				this.rigidbody.velocity = jumpPower * Vector3.up;
			}
		}

		if (cs.gameover) {
			if (Input.anyKey){
				Application.LoadLevel(Application.loadedLevel);
			}
		}

		if (cs.paused) {
			if (Input.anyKey){
				cs.ContinueGame();
			}
		}

		if (cs.finished) {
			Health h = this.gameObject.GetComponent<Health>();
			h.HealthRate = 0;
		}
	}
	

	void OnCollisionExit(Collision other){
		//Debug.Log ("Here");
		if (other.collider.gameObject.tag == "HorizontalPlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			movingOnPlatform = false;
			//Debug.Log ("Here2222");
			
		}
		else if (other.collider.gameObject.tag == "VerticalPlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			movingOnVerticalPlatform = false;
			//Debug.Log ("Here2222");
			
		}
		else if (other.collider.gameObject.tag == "RotatePlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			OnRotatePlatform = false;
			//Debug.Log ("Here2222");
			
		}
	}
	
	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");
		Vector3 velo = this.rigidbody.velocity;
		
		
		/*
		//This block is for if we want graduate starting/ending/turning
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody.velocity.x < horizontalMaxSpeed)
			rigidbody.AddForce(Vector3.right * h * moveForce);
		
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody.velocity.x) > horizontalMaxSpeed)
			rigidbody.velocity = new Vector3(Mathf.Sign(rigidbody.velocity.x) * horizontalMaxSpeed, rigidbody.velocity.y, 0);
		*/
	}
	
	void OnCollisionEnter(Collision coll)
	{
		if(coll.collider.gameObject.layer == LayerMask.NameToLayer("RigidTerrain"))
		{
			if(leftBlocked || rightBlocked)
			{
				this.rigidbody.velocity = Vector3.zero;
			}
		}
		if (coll.collider.gameObject.tag == "HorizontalPlatform"){
			lc = coll.collider.gameObject.GetComponent<LightCube>();
			movingOnPlatform = true;
			//Debug.Log ("Here2222");
			
		}
		if (coll.collider.gameObject.tag == "VerticalPlatform"){
			lc = coll.collider.gameObject.GetComponent<LightCube>();
			movingOnVerticalPlatform = true;
			
		}

		else if (coll.collider.gameObject.tag == "RotatePlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			OnRotatePlatform = true;
			//Debug.Log ("Here2222");
			
		}
	}



	void OnGUI()
	{
		/*
		GUI.Label (new Rect (10, 10, 200, 20), "Grounded:     " + grounded.ToString ());
		GUI.Label (new Rect (10, 30, 200, 20), "LeftBlocked:  " + leftBlocked.ToString ());
		GUI.Label (new Rect (10, 50, 200, 20), "RightBlocked: " + rightBlocked.ToString ());
		*/
	}
}
