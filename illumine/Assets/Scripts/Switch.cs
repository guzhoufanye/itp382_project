﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {
	public GameObject[] MyLights;
	public bool isOn;
	// Use this for initialization
	void Start () {
		isOn = false;
	}
	
	// Update is called once per frame
	void Update () {


	}

	void OnCollisionEnter(Collision other) {

		if (other.collider.gameObject.tag == "Player") {
			Debug.Log ("Trigger Enter" + MyLights.Length);
			for (int i=0; i<MyLights.Length; i++){
				Transform t = MyLights[i].transform;
				MyLights[i].renderer.enabled = !MyLights[i].renderer.enabled;
				isOn = MyLights[i].renderer.enabled;
			}
			Health hs = other.collider.gameObject.GetComponent<Health>();
			hs.InLight(false);
		
		}
		Cutscene cs = Camera.main.GetComponent<Cutscene> ();
		if (cs.LevelNumber == 1){
			cs.TutorialStage = 2;
		}
	}
}
