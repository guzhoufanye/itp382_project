﻿using UnityEngine;
using System.Collections;

public class PlayerBoundCheck : MonoBehaviour {

	//If the script is attached to leftBound
	public bool isLeft;
	public PlayerController pc;
	private Bounds bound;
	private bool isBlocked = false;
	private GameObject[] collidables;

	void Awake()
	{
		bound = this.collider.bounds;
	}

	void Start () 
	{
		collidables = GameObject.FindGameObjectsWithTag ("Platform");
	}
	
	// Update is called once per frame
	void Update () 
	{
		isBlocked = false;
		bound = this.collider.bounds;

		foreach (GameObject c in collidables)
		{
			if(bound.Intersects(c.collider.bounds))
			{
				isBlocked = true;
			}
		}

		if(isLeft)
		{
			pc.leftBlocked = isBlocked;
		}
		else
		{
			pc.rightBlocked = isBlocked;
		}


	}
}
