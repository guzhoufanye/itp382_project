﻿using UnityEngine;
using System.Collections;

public class LightCollider : MonoBehaviour {
	public bool ini_state;
	// Use this for initialization
	void Start () {
		renderer.enabled = ini_state;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			if (renderer.enabled){
				Health hs = other.gameObject.GetComponent<Health>();
				hs.InLight(true);
				Cutscene cs = Camera.main.GetComponent<Cutscene>();
				if (cs.TutorialStage == 0){
					cs.TutorialStage = 1;
				}
			}

		}
		else if (other.gameObject.tag == "LightCube"|| other.gameObject.tag == "RotatePlatform"){

			if (renderer.enabled){
				LightCube lc = other.gameObject.GetComponent<LightCube>();
				if (renderer.enabled){

					lc.ShouldMove = true;
				}
				else{
					lc.ShouldMove = false;
				}
			}
		}
	}

	void OnTriggerStay(Collider other){
		if (other.gameObject.tag == "Player") {
			Health hs = other.gameObject.GetComponent<Health>();
			if (renderer.enabled){
				hs.InLight(true);
				Cutscene cs = Camera.main.GetComponent<Cutscene>();
				if (cs.TutorialStage == 0){
					cs.TutorialStage = 1;
				}
			}
			
		}
		else if (other.gameObject.tag == "LightCube" || other.gameObject.tag == "RotatePlatform"){
			//Debug.Log ("Stay disable");
			LightCube lc = other.gameObject.GetComponent<LightCube>();
			if (renderer.enabled){
				//Debug.Log("Stay");
				lc.ShouldMove = true;
			}
			else{
				lc.ShouldMove = false;
			}
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == "Player") {
			if (renderer.enabled){
				Health hs = other.gameObject.GetComponent<Health>();
				hs.InLight(false);
			}
			
		}

		if (other.gameObject.tag == "LightCube"|| other.gameObject.tag == "RotatePlatform"){
			//Debug.Log ("Stay disable");
			LightCube lc = other.gameObject.GetComponent<LightCube>();
			if (renderer.enabled){
				lc.ShouldMove = false;

			}
		}
	}

}
