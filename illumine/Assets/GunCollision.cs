﻿using UnityEngine;
using System.Collections;

public class GunCollision : MonoBehaviour {
	LightCube lc;
	public bool movingOnPlatform;
	public bool movingOnVerticalPlatform;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (movingOnPlatform) {
			
			float s = 0-lc.currentSpeed;
			if (lc.right){
				s = 0-lc.currentSpeed;
			}
			this.transform.position += Vector3.left * s * Time.deltaTime;
		}
		
		if (movingOnVerticalPlatform) {
			
			float s = 0-lc.currentSpeed;
			this.transform.position -= Vector3.up * s * Time.deltaTime;
		}
	}

	
	void OnCollisionEnter(Collision coll)
	{
		if (coll.collider.gameObject.tag == "HorizontalPlatform"){
			lc = coll.collider.gameObject.GetComponent<LightCube>();
			movingOnPlatform = true;
			//Debug.Log ("Here2222");
			
		}
		if (coll.collider.gameObject.tag == "VerticalPlatform"){
			lc = coll.collider.gameObject.GetComponent<LightCube>();
			movingOnVerticalPlatform = true;
			
		}
	}
	
	void OnCollisionExit(Collision other){
		//Debug.Log ("Here");
		if (other.collider.gameObject.tag == "HorizontalPlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			movingOnPlatform = false;
			//Debug.Log ("Here2222");
			
		}
		else if (other.collider.gameObject.tag == "VerticalPlatform"){
			//LightCube lc = other.collider.gameObject.GetComponent<LightCube>();
			movingOnVerticalPlatform = false;
			//Debug.Log ("Here2222");
			
		}
	}

}
