# ILLUMINE #


### ITP 382 Team Project ###

The game is a 3D platformer with 2D gameplay where you would try to stay in the bright areas. Many things in the game would function as light source, such as window and light bulb. The dark areas are mysterious and dangerous to player (you get hurt, flower turns from bright to wither color). Throughout the game, there would be places where the player has to use light source such as flare gun to help himself crossing those dark areas. In the end the game would be a half puzzle half action platformer.

### Members ###

* Yanqi Niu (Roy): yanqiniu@usc.edu
* Yunying Tu: yunyingt@usc.edu
* Richard Fu: fur@usc.edu

### 3rd Party Assets Used ###

* Volumetric Light Package, Skyboxes, Platform materials, 3D robot model bought from Unity Store
* Forest background texture in the start scene and button textures downloaded from google.com
* Background music comes from Monument Valley